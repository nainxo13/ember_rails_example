# For more information see: http://emberjs.com/guides/routing/

EmberApp.Router.map ()->
   @resource 'posts'
   @resource('post', { path: '/posts/:post_id'})
   @route('posts.new', { path: '/posts/new'})
   @route('posts.edit', { path: '/posts/:post_id/edit'})

