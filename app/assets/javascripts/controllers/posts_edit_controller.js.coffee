# for more details see: http://emberjs.com/guides/controllers/

EmberApp.PostsEditController = Ember.Controller.extend({
  actions:
    update: ->
      @model.save()
      @transitionToRoute('post', @model)
})

