# For more information see: http://emberjs.com/guides/routing/

EmberApp.PostsNewRoute = Ember.Route.extend({
  model: ->
    this.store.createRecord('post')

  setupController: (controller, model) ->
    controller.set('model', model)
})
