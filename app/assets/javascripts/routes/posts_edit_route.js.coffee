# For more information see: http://emberjs.com/guides/routing/

EmberApp.PostsEditRoute = Ember.Route.extend({
  model: ->
    @store.find('post',params.post_id)

  setupController: (controller, model) ->
    controller.set('model', model)
})
